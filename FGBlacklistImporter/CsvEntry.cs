﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGBlacklistImporter
{
    class CsvEntry
    {
        public ulong ReporterId { get; }
        public DateTime ReportDate { get; }
        public string IngameName { get; }
        public string SteamId { get; }
        public string PlayfabId { get; }
        public string Reason { get; }

        public CsvEntry(string csvLine)
        {
            string[] csvElements = csvLine.Split(';');

            ReporterId = ulong.Parse(csvElements[0]);
            ReportDate = DateTime.Parse(csvElements[1]);
            IngameName = csvElements[2];
            SteamId = csvElements[3];
            PlayfabId = csvElements[4];
            Reason = csvElements[5];
        }

        public void InsertUserIntoDb(FreeGuardDbContext context, int guildId)
        {
            if (!context.Discordusers.Any(x => x.DiscordUserId == ReporterId))
            {
                Discorduser discorduser = new()
                {
                    DiscordUserId = ReporterId,
                    CreateDate = DateTime.UtcNow
                };
                context.Discordusers.Add(discorduser);
                context.SaveChanges();
            }

            if (!context.Guildusers.Any(x => x.DiscordUserId == ReporterId && x.GuildId == guildId))
            {
                Guilduser guilduser = new()
                {
                    GuildId = guildId,
                    DiscordUserId = ReporterId,
                    CreateDate = DateTime.UtcNow,
                };

                context.Guildusers.Add(guilduser);
                context.SaveChanges();
            }
        }

        public void InsertBlacklistIntoDb(FreeGuardDbContext context, int guildId)
        {
            if (!context.Blacklistentries.Any(x => x.GuildId == guildId && x.SteamId == SteamId))
            {
                Blacklistentry blacklistentry = new()
                {
                    Username = IngameName,
                    SteamId = SteamId,
                    PlayfabId = PlayfabId,
                    Reason = Reason,
                    GuildId = guildId,
                    CreateDate = ReportDate,
                    CreateUserId = ReporterId
                };

                context.Blacklistentries.Add(blacklistentry);
                context.SaveChanges();
            }  
        }
    }
}

# Freeguard Bot:

## General idea:

A bot which manages the blacklist and maybe other stuff like the recruiting.

## Suggested/Planned features (Short list):

F = feature
O = optional

 - F1: The bot can be used to create blacklist entries
 - F2: The bot can be used to manage blacklist entries
 - F3: The bot can be used to delete blacklist entries (not prefered)
 - F4: The bot rights can be bound to existing roles
 - F5: The bot can be used to search entries
 - F6: The bot checks the SteamId of new recruits against the blacklist
 - F7: The bot can use the lookup player string to create blacklist entries
 - F8: The blacklist entries can be extended with additional alias names of blacklisted players
 - F9: The blacklist will be centralized managed by the bot, means the entries would be saved in a db by the bot. (no more excel and ds chat manual management)
 - F10: The existing entries would be migrated into the database
 - O1: The bot could be used to manage the recruitment
 - O2: The bot could be used to either mark or ban blacklisted players when the discord name is known

## Important to discuss 

 - Rather private channel for creation or creation in channel with deletion of messages afterwards?
 
## Planned Technical details

 - Opensource
 - Database: MySql
 - .Net 5 based (i know you can use javascript but i hate javascript.)
 - DSharpPlus as a framework for the bot development
 - Dapper as an ORM Mapper since i am too lazy to setup entity framework core with mysql (would be better tho)
 - C#
 - Repository would be on a gitlab acc and would be set to public so everyone who wants can submit merge requests.
 - Hosting details would be thought of later since i would like to set up an deployment system for this so it makes development and delivery easier.

## Features in more detail:

### F1 Blacklist entry creation:
---

#### Description:

A command which can be used to create entries in the blacklist. The data it needs will either be exchanged in a private channel or in the channel of the execution of the command and then delete the messages afterwards. It is possible to give it a lookup player to get nearly all required data with the exception of the reason.

#### Required data:

 - Playername (required)
 - steamId (required)
 - playfabId (optional)
 - reason (required)

### F2 Blacklist entry management:
---

#### Description:

A collection of commands which will allow to change data in the blacklist. Does not include the command for deletion of entries. 

#### Required data:

 - depends on the information that is changed

### F3 Blacklist entry deletion:
---

#### Description:

A command for deleting blacklist entries. This should not easily be allowed since its always better to keep entries and mark them as revoked then lose data completely.

#### Require data:

 - Blacklist entry id

### F4 Right system: 
---

#### Description:

The bot gets its own right system which determine what can be used. These rights can be bound to a role so this role gets these rights in the bot. There will be commands for setting rights.

#### Required data:

 - discord roles

### F5 Entry search:
---

#### Description:

The bot gets commands to search entries based on entry id, name, playfabid and steamid.

#### Required data:

 - blacklist entry id 
 - name
 - playfabid
 - steamid
 - loopup player string

### F6 Recruitment check:
---

#### Description:

The bot checks when new recruits are added wether they are on blacklist or not. When on blacklist they get market and a message get send to the recruiter or other relevant persons.

#### Required data:

 - recruitment information

### F7 Lookup player usage
---

#### Description:

The features #5 and #1 can parse the lookup player string to make it more easy to create new entries.

#### Required data:

 - Loopup player string

### F8 Alias list
---

#### Description:

Blacklist entries can get extended with additional alias names. Could be a single line command which deletes itself.

#### Required data:

 - entry id
 - alias name

### F9 Centralized data management:
---

#### Description:

Instead of the discord channel and the excel file which are combined the blacklist all data will be managed by the bot so no human interaction to keep things synchronized should be needed. The excel file will be unnessesary.

#### Required data:

 - none

### F10 Excel and discord to DB migration:
---
 
#### Description

The discord and the excel entries will be migrated into the database.

### O1 Recruitment management:
---

The bot would be capable of manage and create recruitment entries and existing recruitment data would be migrated. Since the bot controls this part to it would make the check of blacklisted people way easier for recruiter as they could be notified easier. This would have to be thought through more but not hard to do either.

### O2 Marking or banning of blacklisted people
---

The bot would be a able to ban or mark blacklisted people when they are recruited. If their discord name would be known it would even be possible to ban or mark them on joining instead of on recruiting.

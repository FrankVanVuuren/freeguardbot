DROP TABLE IF EXISTS Vacation;
DROP TABLE IF EXISTS RoleRight;
DROP TABLE IF EXISTS Role;
DROP TABLE IF EXISTS BotRight;
DROP TABLE IF EXISTS Recruit;
DROP TABLE IF EXISTS Alias;
DROP TABLE IF EXISTS Confirmation;
DROP TABLE IF EXISTS BlacklistEntry;
DROP TABLE IF EXISTS GuildUser;
DROP TABLE IF EXISTS DiscordUser;
DROP TABLE IF EXISTS CommandInfo;
DROP TABLE IF EXISTS Guild;

CREATE TABLE IF NOT EXISTS Guild (
    Id INT auto_increment,
    GuildId BIGINT UNSIGNED NOT NULL,
    CreateDate TIMESTAMP NOT NULL,
    InitialName VARCHAR(255) NOT NULL,
    RecruitmentChannelId BIGINT UNSIGNED,
    PRIMARY KEY (Id)
);

CREATE TABLE IF NOT EXISTS CommandInfo (
    Id INT auto_increment,
    GuildId INT NOT NULL,
    CommandName VARCHAR(255) NOT NULL,
    Paramether VARCHAR(255),
    CreateDate TIMESTAMP NOT NULL,
    PRIMARY KEY (Id),
    FOREIGN KEY (GuildId) REFERENCES Guild(Id)
);

CREATE TABLE IF NOT EXISTS DiscordUser  (
    DiscordUserId BIGINT UNSIGNED,
    CreateDate TIMESTAMP NOT NULL,
    PRIMARY KEY (DiscordUserId)
);

CREATE TABLE IF NOT EXISTS GuildUser (
    Id INT auto_increment,
    GuildId INT NOT NULL,
    DiscordUserId BIGINT UNSIGNED NOT NULL,
    CreateDate TIMESTAMP NOT NULL,
    PRIMARY KEY (Id),
    FOREIGN KEY (GuildId) REFERENCES Guild(Id),
    FOREIGN KEY (DiscordUserId) REFERENCES DiscordUser(DiscordUserId)
);

CREATE TABLE IF NOT EXISTS BlacklistEntry (
    Id INT auto_increment,
    Username VARCHAR(50) NOT NULL,
    SteamId VARCHAR(255) NOT NULL,
    PlayfabId VARCHAR(255),
    Reason VARCHAR(255) NOT NULL,
    GuildId INT NOT NULL,
    CreateDate TIMESTAMP NOT NULL,
    CreateUserId BIGINT UNSIGNED NOT NULL,
    ModifyDate TIMESTAMP,
    ModifyUserId BIGINT UNSIGNED,
    PRIMARY KEY (Id),
    FOREIGN KEY (CreateUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (ModifyUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (GuildId) REFERENCES Guild(Id)
);

CREATE TABLE IF NOT EXISTS Confirmation (
    Id INT auto_increment,
    ConfirmUserId BIGINT UNSIGNED NOT NULL,
    BlacklistEntryId int NOT NULL,
    CreateDate TIMESTAMP NOT NULL,
    CreateUserId BIGINT UNSIGNED NOT NULL,
    ModifyDate TIMESTAMP,
    ModifyUserId BIGINT UNSIGNED,
    PRIMARY KEY (Id),
    FOREIGN KEY (ConfirmUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (BlacklistEntryId) REFERENCES BlacklistEntry(Id),
    FOREIGN KEY (CreateUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (ModifyUserId) REFERENCES DiscordUser(DiscordUserId)
);

CREATE TABLE IF NOT EXISTS Alias (
    Id INT auto_increment,
    Username VARCHAR(50) NOT NULL,
    BlacklistEntryId INT NOT NULL,
    CreateDate TIMESTAMP NOT NULL,
    CreateUserId BIGINT UNSIGNED NOT NULL,
    ModifyDate TIMESTAMP,
    ModifyUserId BIGINT UNSIGNED,
    PRIMARY KEY (Id),
    FOREIGN KEY (CreateUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (ModifyUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (BlacklistEntryId) REFERENCES BlacklistEntry(Id)
);

CREATE TABLE IF NOT EXISTS Recruit (
    Id INT auto_increment,
    RecruitId BIGINT UNSIGNED,
    RecruiterId BIGINT UNSIGNED,
    GuildId INT NOT NULL,
    Name VARCHAR(255) NOT NULL,
    DiscordName VARCHAR(255) NOT NULL,
    SteamId VARCHAR(255) NOT NULL,
    Joined VARCHAR (5) NOT NULL,
    CreateDate TIMESTAMP NOT NULL,
    CreateUserId BIGINT UNSIGNED NOT NULL,
    ModifyDate TIMESTAMP,
    ModifyUserId BIGINT UNSIGNED,
    PRIMARY KEY (Id),
    FOREIGN KEY (RecruitId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (RecruiterId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (CreateUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (ModifyUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (GuildId) REFERENCES Guild(Id)
);

CREATE TABLE IF NOT EXISTS BotRight (
    Id INT, 
    Name VARCHAR(50) NOT NULL,
    Description VARCHAR(255) NOT NULL,
    PRIMARY KEY (Id)
);

CREATE TABLE IF NOT EXISTS Role (
    Id INT auto_increment,
    DiscordRoleId BIGINT UNSIGNED NOT NULL,
    GuildId INT NOT NULL,
    IsJoinRole VARCHAR(5) DEFAULT 'false' NOT NULL,
    IsWarningRole VARCHAR(5) DEFAULT 'false' NOT NULL,
    CreateDate TIMESTAMP NOT NULL,
    CreateUserId BIGINT UNSIGNED NOT NULL,
    ModifyDate TIMESTAMP,
    ModifyUserId BIGINT UNSIGNED,
    PRIMARY KEY (Id),
    FOREIGN KEY (CreateUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (ModifyUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (GuildId) REFERENCES Guild(Id)
);

CREATE TABLE IF NOT EXISTS RoleRight (
    Id INT auto_increment,
    RoleId INT NOT NULL,
    BotRightId INT NOT NULL,
    CreateDate TIMESTAMP NOT NULL,
    CreateUserId BIGINT UNSIGNED NOT NULL,
    ModifyDate TIMESTAMP,
    ModifyUserId BIGINT UNSIGNED,
    PRIMARY KEY (Id),
    FOREIGN KEY (CreateUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (ModifyUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (RoleId) REFERENCES Role(Id),
    FOREIGN KEY (BotRightId) REFERENCES BotRight(Id)
);

CREATE TABLE IF NOT EXISTS Vacation (
    Id INT auto_increment,
    AffectedId BIGINT UNSIGNED NOT NULL,
    FreezeTitleId INT NOT NULL,
    GuildId INT NOT NULL,
    StartDate TIMESTAMP,
    EstimatedDuration INT,
    Reason VARCHAR(255) NOT NULL,
    Finished VARCHAR (5),
    CreateDate TIMESTAMP NOT NULL,
    CreateUserId BIGINT UNSIGNED NOT NULL,
    ModifyDate TIMESTAMP,
    ModifyUserId BIGINT UNSIGNED,
    PRIMARY KEY (Id),
    FOREIGN KEY (AffectedId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (FreezeTitleId) REFERENCES Role(Id),
    FOREIGN KEY (CreateUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (ModifyUserId) REFERENCES DiscordUser(DiscordUserId),
    FOREIGN KEY (GuildId) REFERENCES Guild(Id)
);

INSERT INTO BotRight 
	(Id, Name, Description)
VALUES
	(1, 'create_entry', 'Right for creating new entries in the blacklist.'),
    (2, 'update_entry', 'Right for updating entries in the blacklist.'),
    (3, 'delete_entry', 'Right for deleting entries in the blacklist.'),
    (4, 'export_list', 'Right for exporting all entries into a csv file.'),
    (5, 'search_entries', 'Right for searching the blacklist for entries.'),
    (6, 'set_rights', 'Right for setting bot rights.'),
    (7, 'del_rights', 'Right for deleting bot rights.'),
    (8, 'manage_entries', 'Groupright for right 1,2,3 and 5. Meant for simple setup of a entry manager role.'),
    (9, 'manage_bot', 'Groupright for right 1,2,3,4,5,6,7,11. Meant for a simple setup of a bot admin role.'),
    (10, 'use_list', 'Groupright for right 1, 5. Meant for a simple setup of a simple user role.'),
    (11, 'view_rights', 'Right for viewing the existing rights.'),
    (12, 'create_recruit', 'Right for creating new recruits.');
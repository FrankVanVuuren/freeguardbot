﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class Guild
    {
        public static async Task<Guild> GetOrCreateGuildAsync(FreeGuardDbContext context, ulong discordGuildId, string guildName)
        {
            var guild = await context.Guilds
                            .Include("Guildusers")
                            .SingleOrDefaultAsync(x => x.GuildId == discordGuildId)
                            .ConfigureAwait(false);

            if (guild is null)
            {
                guild = new Guild()
                {
                    GuildId = discordGuildId,
                    CreateDate = DateTime.UtcNow,
                    InitialName = guildName,
                };

                await context.AddAsync(guild);
                await context.SaveChangesAsync();
            }

            return guild;
        }

        public async static Task<Guild> GetAsync(FreeGuardDbContext context, ulong discordGuildId)
        {
            return await context.Guilds
               .SingleOrDefaultAsync(x => x.GuildId == discordGuildId);
        }
    }
}

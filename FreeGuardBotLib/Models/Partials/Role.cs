﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class Role
    {
        public bool HasWarning
        {
            get
            {
                return IsWarningRole == "true";
            }
        }

        public bool HasJoin
        {
            get
            {
                return IsJoinRole == "true";
            }
        }

        public async static Task<Role> GetOrCreateRoleAsync(FreeGuardDbContext dbContext, ulong roleId, ulong createUserId, int guildId)
        {
            var role = await dbContext.Roles
                .SingleOrDefaultAsync(x => x.GuildId == guildId && x.DiscordRoleId == roleId)
                .ConfigureAwait(false);

            if (role is null)
            {
                role = new Role()
                {
                    GuildId = guildId,
                    CreateDate = DateTime.UtcNow,
                    DiscordRoleId = roleId,
                    CreateUserId = createUserId,
                };

                await dbContext.AddAsync(role);
                await dbContext.SaveChangesAsync();
            }

            return role;
        }
    }
}

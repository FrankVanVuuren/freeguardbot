﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public enum Rights
    {
        CreateEntry = 1,
        UpdateEntry = 2,
        DeleteEntry = 3,
        ExportList = 4,
        SearchEntries = 5,
        SetRights = 6,
        DeleteRights = 7,
        ManageEntries = 8,
        ManageBot = 9,
        UseList = 10,
        ViewRights = 11,
        CreateRecruit = 12,
    }

    public partial class Botright
    {
        public async static Task<Botright> GetCreateEntryRightAsync(FreeGuardDbContext ctx)
        {
            return await ctx.Botrights
                .FindAsync((int)Rights.CreateEntry);
        }

        public async static Task<Botright> GetUpdateEntryRightAsync(FreeGuardDbContext ctx)
        {
            return await ctx.Botrights
                .FindAsync((int)Rights.UpdateEntry);
        }

        public async static Task<Botright> GetDeleteEntryRightAsync(FreeGuardDbContext ctx)
        {
            return await ctx.Botrights
                .FindAsync((int)Rights.DeleteEntry);
        }

        public async static Task<Botright> GetExportListRightAsync(FreeGuardDbContext ctx)
        {
            return await ctx.Botrights
                .FindAsync((int)Rights.ExportList);
        }

        public async static Task<Botright> GetSearchEntriesRightAsync(FreeGuardDbContext ctx)
        {
            return await ctx.Botrights
                .FindAsync((int)Rights.SearchEntries);
        }

        public async static Task<Botright> GetSetRightsRightAsync(FreeGuardDbContext ctx)
        {
            return await ctx.Botrights
                .FindAsync((int)Rights.SetRights);
        }

        public async static Task<Botright> GetManageEntriesRightAsync(FreeGuardDbContext ctx)
        {
            return await ctx.Botrights
                .FindAsync((int)Rights.ManageEntries);
        }

        public async static Task<Botright> GetManageBotRightAsync(FreeGuardDbContext ctx)
        {
            return await ctx.Botrights
                .FindAsync((int)Rights.ManageBot);
        }

        public async static Task<Botright> GetDeleteRightsRightAsync(FreeGuardDbContext ctx)
        {
            return await ctx.Botrights
                .FindAsync((int)Rights.DeleteRights);
        }

        public async static Task<Botright> GetUseListRightAsync(FreeGuardDbContext ctx)
        {
            return await ctx.Botrights
                .FindAsync((int)Rights.UseList);
        }

        public async static Task<Botright> GetViewRightsRightAsync(FreeGuardDbContext ctx)
        {
            return await ctx.Botrights
                .FindAsync((int)Rights.ViewRights);
        }
    }
}

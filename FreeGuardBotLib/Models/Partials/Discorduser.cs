﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class Discorduser
    {
        public static async Task<Discorduser> GetOrCreateDiscorduserAsync(FreeGuardDbContext context, ulong userId)
        {
            var user = await context.Discordusers
                            .SingleOrDefaultAsync(x => x.DiscordUserId == userId)
                            .ConfigureAwait(false);

            if (user is null)
            {
                user = new Discorduser()
                {
                    DiscordUserId = userId,
                    CreateDate = DateTime.UtcNow
                };

                await context.AddAsync(user);
                await context.SaveChangesAsync();
            }

            return user;
        }
    }
}

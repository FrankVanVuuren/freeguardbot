﻿using DSharpPlus.CommandsNext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class Commandinfo
    {
        public static async Task LogCommandAsync(FreeGuardDbContext dbContext, int guildId, string commandName)
        {
            await LogCommandAsync(dbContext, guildId, commandName, null);
        }

        public static async Task LogCommandAsync(FreeGuardDbContext dbContext, int guildId, string commandName, string parameter)
        {
            var commandInfo = new Commandinfo()
            {
                CommandName = commandName,
                Paramether = parameter,
                GuildId = guildId,
                CreateDate = DateTime.UtcNow,
            };

            await dbContext.AddAsync(commandInfo);
            await dbContext.SaveChangesAsync();
        }
    }
}

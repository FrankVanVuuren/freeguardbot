﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class Guilduser
    {
        public static async Task GetOrCreateGuildUserAsync(FreeGuardDbContext context, int guildId, ulong discordUserId)
        {
            var guildUser = await context.Guildusers
                            .SingleOrDefaultAsync(x => x.GuildId == guildId && x.DiscordUserId == discordUserId)
                            .ConfigureAwait(false);

            if (guildUser is null)
            {
                guildUser = new Guilduser()
                {
                    GuildId = guildId,
                    DiscordUserId = discordUserId,
                    CreateDate = DateTime.UtcNow
                };

                await context.AddAsync(guildUser);
                await context.SaveChangesAsync();
            }
        }
    }
}

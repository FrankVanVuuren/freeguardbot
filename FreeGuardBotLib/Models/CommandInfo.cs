﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models
{
    public partial class Commandinfo
    {
        public int Id { get; set; }
        public int GuildId { get; set; }
        public string CommandName { get; set; }
        public string Paramether { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual Guild Guild { get; set; }
    }
}

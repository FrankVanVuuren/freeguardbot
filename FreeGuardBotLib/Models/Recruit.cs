﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models
{
    public partial class Recruit
    {
        public int Id { get; set; }
        public ulong? RecruitId { get; set; }
        public ulong? RecruiterId { get; set; }
        public int GuildId { get; set; }
        public string Name { get; set; }
        public string DiscordName { get; set; }
        public string SteamId { get; set; }
        public string Joined { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public ulong? ModifyUserId { get; set; }

        public virtual Discorduser CreateUser { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
        public virtual Discorduser RecruitNavigation { get; set; }
        public virtual Discorduser Recruiter { get; set; }
    }
}

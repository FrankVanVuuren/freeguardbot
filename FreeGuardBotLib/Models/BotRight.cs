﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models
{
    public partial class Botright
    {
        public Botright()
        {
            Rolerights = new HashSet<Roleright>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Roleright> Rolerights { get; set; }
    }
}

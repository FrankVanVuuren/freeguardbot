﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models
{
    public partial class Guilduser
    {
        public int Id { get; set; }
        public int GuildId { get; set; }
        public ulong DiscordUserId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual Discorduser DiscordUser { get; set; }
        public virtual Guild Guild { get; set; }
    }
}

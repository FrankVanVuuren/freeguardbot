﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models
{
    public partial class Role
    {
        public Role()
        {
            Rolerights = new HashSet<Roleright>();
            Vacations = new HashSet<Vacation>();
        }

        public int Id { get; set; }
        public ulong DiscordRoleId { get; set; }
        public int GuildId { get; set; }
        public string IsJoinRole { get; set; }
        public string IsWarningRole { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public ulong? ModifyUserId { get; set; }

        public virtual Discorduser CreateUser { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
        public virtual ICollection<Roleright> Rolerights { get; set; }
        public virtual ICollection<Vacation> Vacations { get; set; }
    }
}

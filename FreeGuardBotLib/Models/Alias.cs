﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models
{
    public partial class Alias
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int BlacklistEntryId { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public ulong? ModifyUserId { get; set; }

        public virtual Blacklistentry BlacklistEntry { get; set; }
        public virtual Discorduser CreateUser { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
    }
}

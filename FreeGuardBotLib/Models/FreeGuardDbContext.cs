﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FreeGuardBotLib.Models
{
    public partial class FreeGuardDbContext : DbContext
    {
        public FreeGuardDbContext()
        {
        }

        public FreeGuardDbContext(DbContextOptions<FreeGuardDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Alias> Aliases { get; set; }
        public virtual DbSet<Blacklistentry> Blacklistentries { get; set; }
        public virtual DbSet<Botright> Botrights { get; set; }
        public virtual DbSet<Commandinfo> Commandinfos { get; set; }
        public virtual DbSet<Confirmation> Confirmations { get; set; }
        public virtual DbSet<Discorduser> Discordusers { get; set; }
        public virtual DbSet<Guild> Guilds { get; set; }
        public virtual DbSet<Guilduser> Guildusers { get; set; }
        public virtual DbSet<Recruit> Recruits { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Roleright> Rolerights { get; set; }
        public virtual DbSet<Vacation> Vacations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(BotConfig.GetInstance().DbConnString, Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.18-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("utf8mb4")
                .UseCollation("utf8mb4_0900_ai_ci");

            modelBuilder.Entity<Alias>(entity =>
            {
                entity.ToTable("Alias");

                entity.HasIndex(e => e.BlacklistEntryId, "BlacklistEntryId");

                entity.HasIndex(e => e.CreateUserId, "CreateUserId");

                entity.HasIndex(e => e.ModifyUserId, "ModifyUserId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BlacklistEntryId).HasColumnType("int(11)");

                entity.Property(e => e.CreateDate).HasColumnType("timestamp");

                entity.Property(e => e.CreateUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.ModifyDate).HasColumnType("timestamp");

                entity.Property(e => e.ModifyUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.BlacklistEntry)
                    .WithMany(p => p.Aliases)
                    .HasForeignKey(d => d.BlacklistEntryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("alias_ibfk_3");

                entity.HasOne(d => d.CreateUser)
                    .WithMany(p => p.AliasCreateUsers)
                    .HasForeignKey(d => d.CreateUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("alias_ibfk_1");

                entity.HasOne(d => d.ModifyUser)
                    .WithMany(p => p.AliasModifyUsers)
                    .HasForeignKey(d => d.ModifyUserId)
                    .HasConstraintName("alias_ibfk_2");
            });

            modelBuilder.Entity<Blacklistentry>(entity =>
            {
                entity.ToTable("BlacklistEntry");

                entity.HasIndex(e => e.CreateUserId, "CreateUserId");

                entity.HasIndex(e => e.GuildId, "GuildId");

                entity.HasIndex(e => e.ModifyUserId, "ModifyUserId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreateDate).HasColumnType("timestamp");

                entity.Property(e => e.CreateUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.GuildId).HasColumnType("int(11)");

                entity.Property(e => e.ModifyDate).HasColumnType("timestamp");

                entity.Property(e => e.ModifyUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.PlayfabId).HasMaxLength(255);

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.SteamId)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.CreateUser)
                    .WithMany(p => p.BlacklistentryCreateUsers)
                    .HasForeignKey(d => d.CreateUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("blacklistentry_ibfk_1");

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Blacklistentries)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("blacklistentry_ibfk_3");

                entity.HasOne(d => d.ModifyUser)
                    .WithMany(p => p.BlacklistentryModifyUsers)
                    .HasForeignKey(d => d.ModifyUserId)
                    .HasConstraintName("blacklistentry_ibfk_2");
            });

            modelBuilder.Entity<Botright>(entity =>
            {
                entity.ToTable("BotRight");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Commandinfo>(entity =>
            {
                entity.ToTable("CommandInfo");

                entity.HasIndex(e => e.GuildId, "GuildId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CommandName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.CreateDate).HasColumnType("timestamp");

                entity.Property(e => e.GuildId).HasColumnType("int(11)");

                entity.Property(e => e.Paramether).HasMaxLength(255);

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Commandinfos)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("commandinfo_ibfk_1");
            });

            modelBuilder.Entity<Confirmation>(entity =>
            {
                entity.ToTable("Confirmation");

                entity.HasIndex(e => e.BlacklistEntryId, "BlacklistEntryId");

                entity.HasIndex(e => e.ConfirmUserId, "ConfirmUserId");

                entity.HasIndex(e => e.CreateUserId, "CreateUserId");

                entity.HasIndex(e => e.ModifyUserId, "ModifyUserId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BlacklistEntryId).HasColumnType("int(11)");

                entity.Property(e => e.ConfirmUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.CreateDate).HasColumnType("timestamp");

                entity.Property(e => e.CreateUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.ModifyDate).HasColumnType("timestamp");

                entity.Property(e => e.ModifyUserId).HasColumnType("bigint(20) unsigned");

                entity.HasOne(d => d.BlacklistEntry)
                    .WithMany(p => p.Confirmations)
                    .HasForeignKey(d => d.BlacklistEntryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("confirmation_ibfk_2");

                entity.HasOne(d => d.ConfirmUser)
                    .WithMany(p => p.ConfirmationConfirmUsers)
                    .HasForeignKey(d => d.ConfirmUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("confirmation_ibfk_1");

                entity.HasOne(d => d.CreateUser)
                    .WithMany(p => p.ConfirmationCreateUsers)
                    .HasForeignKey(d => d.CreateUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("confirmation_ibfk_3");

                entity.HasOne(d => d.ModifyUser)
                    .WithMany(p => p.ConfirmationModifyUsers)
                    .HasForeignKey(d => d.ModifyUserId)
                    .HasConstraintName("confirmation_ibfk_4");
            });

            modelBuilder.Entity<Discorduser>(entity =>
            {
                entity.ToTable("DiscordUser");

                entity.Property(e => e.DiscordUserId)
                    .HasColumnType("bigint(20) unsigned")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateDate).HasColumnType("timestamp");
            });

            modelBuilder.Entity<Guild>(entity =>
            {
                entity.ToTable("Guild");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreateDate).HasColumnType("timestamp");

                entity.Property(e => e.GuildId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.InitialName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.RecruitmentChannelId).HasColumnType("bigint(20) unsigned");
            });

            modelBuilder.Entity<Guilduser>(entity =>
            {
                entity.ToTable("GuildUser");

                entity.HasIndex(e => e.DiscordUserId, "DiscordUserId");

                entity.HasIndex(e => e.GuildId, "GuildId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreateDate).HasColumnType("timestamp");

                entity.Property(e => e.DiscordUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.GuildId).HasColumnType("int(11)");

                entity.HasOne(d => d.DiscordUser)
                    .WithMany(p => p.Guildusers)
                    .HasForeignKey(d => d.DiscordUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("guilduser_ibfk_2");

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Guildusers)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("guilduser_ibfk_1");
            });

            modelBuilder.Entity<Recruit>(entity =>
            {
                entity.ToTable("recruit");

                entity.HasIndex(e => e.CreateUserId, "CreateUserId");

                entity.HasIndex(e => e.GuildId, "GuildId");

                entity.HasIndex(e => e.ModifyUserId, "ModifyUserId");

                entity.HasIndex(e => e.RecruitId, "RecruitId");

                entity.HasIndex(e => e.RecruiterId, "RecruiterId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreateDate).HasColumnType("timestamp");

                entity.Property(e => e.CreateUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.DiscordName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.GuildId).HasColumnType("int(11)");

                entity.Property(e => e.Joined)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.ModifyDate).HasColumnType("timestamp");

                entity.Property(e => e.ModifyUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.RecruitId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.RecruiterId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.SteamId)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.CreateUser)
                    .WithMany(p => p.RecruitCreateUsers)
                    .HasForeignKey(d => d.CreateUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("recruit_ibfk_3");

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Recruits)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("recruit_ibfk_5");

                entity.HasOne(d => d.ModifyUser)
                    .WithMany(p => p.RecruitModifyUsers)
                    .HasForeignKey(d => d.ModifyUserId)
                    .HasConstraintName("recruit_ibfk_4");

                entity.HasOne(d => d.RecruitNavigation)
                    .WithMany(p => p.RecruitRecruitNavigations)
                    .HasForeignKey(d => d.RecruitId)
                    .HasConstraintName("recruit_ibfk_1");

                entity.HasOne(d => d.Recruiter)
                    .WithMany(p => p.RecruitRecruiters)
                    .HasForeignKey(d => d.RecruiterId)
                    .HasConstraintName("recruit_ibfk_2");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.HasIndex(e => e.CreateUserId, "CreateUserId");

                entity.HasIndex(e => e.GuildId, "GuildId");

                entity.HasIndex(e => e.ModifyUserId, "ModifyUserId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreateDate).HasColumnType("timestamp");

                entity.Property(e => e.CreateUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.DiscordRoleId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.GuildId).HasColumnType("int(11)");

                entity.Property(e => e.IsJoinRole)
                    .IsRequired()
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'false'");

                entity.Property(e => e.IsWarningRole)
                    .IsRequired()
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'false'");

                entity.Property(e => e.ModifyDate).HasColumnType("timestamp");

                entity.Property(e => e.ModifyUserId).HasColumnType("bigint(20) unsigned");

                entity.HasOne(d => d.CreateUser)
                    .WithMany(p => p.RoleCreateUsers)
                    .HasForeignKey(d => d.CreateUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("role_ibfk_1");

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Roles)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("role_ibfk_3");

                entity.HasOne(d => d.ModifyUser)
                    .WithMany(p => p.RoleModifyUsers)
                    .HasForeignKey(d => d.ModifyUserId)
                    .HasConstraintName("role_ibfk_2");
            });

            modelBuilder.Entity<Roleright>(entity =>
            {
                entity.ToTable("RoleRight");

                entity.HasIndex(e => e.BotRightId, "BotRightId");

                entity.HasIndex(e => e.CreateUserId, "CreateUserId");

                entity.HasIndex(e => e.ModifyUserId, "ModifyUserId");

                entity.HasIndex(e => e.RoleId, "RoleId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BotRightId).HasColumnType("int(11)");

                entity.Property(e => e.CreateDate).HasColumnType("timestamp");

                entity.Property(e => e.CreateUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.ModifyDate).HasColumnType("timestamp");

                entity.Property(e => e.ModifyUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.RoleId).HasColumnType("int(11)");

                entity.HasOne(d => d.BotRight)
                    .WithMany(p => p.Rolerights)
                    .HasForeignKey(d => d.BotRightId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("roleright_ibfk_4");

                entity.HasOne(d => d.CreateUser)
                    .WithMany(p => p.RolerightCreateUsers)
                    .HasForeignKey(d => d.CreateUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("roleright_ibfk_1");

                entity.HasOne(d => d.ModifyUser)
                    .WithMany(p => p.RolerightModifyUsers)
                    .HasForeignKey(d => d.ModifyUserId)
                    .HasConstraintName("roleright_ibfk_2");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Rolerights)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("roleright_ibfk_3");
            });

            modelBuilder.Entity<Vacation>(entity =>
            {
                entity.ToTable("vacation");

                entity.HasIndex(e => e.AffectedId, "AffectedId");

                entity.HasIndex(e => e.CreateUserId, "CreateUserId");

                entity.HasIndex(e => e.FreezeTitleId, "FreezeTitleId");

                entity.HasIndex(e => e.GuildId, "GuildId");

                entity.HasIndex(e => e.ModifyUserId, "ModifyUserId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AffectedId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.CreateDate).HasColumnType("timestamp");

                entity.Property(e => e.CreateUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.EstimatedDuration).HasColumnType("int(11)");

                entity.Property(e => e.Finished).HasMaxLength(5);

                entity.Property(e => e.FreezeTitleId).HasColumnType("int(11)");

                entity.Property(e => e.GuildId).HasColumnType("int(11)");

                entity.Property(e => e.ModifyDate).HasColumnType("timestamp");

                entity.Property(e => e.ModifyUserId).HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.StartDate).HasColumnType("timestamp");

                entity.HasOne(d => d.Affected)
                    .WithMany(p => p.VacationAffecteds)
                    .HasForeignKey(d => d.AffectedId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("vacation_ibfk_1");

                entity.HasOne(d => d.CreateUser)
                    .WithMany(p => p.VacationCreateUsers)
                    .HasForeignKey(d => d.CreateUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("vacation_ibfk_3");

                entity.HasOne(d => d.FreezeTitle)
                    .WithMany(p => p.Vacations)
                    .HasForeignKey(d => d.FreezeTitleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("vacation_ibfk_2");

                entity.HasOne(d => d.Guild)
                    .WithMany(p => p.Vacations)
                    .HasForeignKey(d => d.GuildId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("vacation_ibfk_5");

                entity.HasOne(d => d.ModifyUser)
                    .WithMany(p => p.VacationModifyUsers)
                    .HasForeignKey(d => d.ModifyUserId)
                    .HasConstraintName("vacation_ibfk_4");
            });
        }
    }
}

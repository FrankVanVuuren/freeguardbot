﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models
{
    public partial class Guild
    {
        public Guild()
        {
            Blacklistentries = new HashSet<Blacklistentry>();
            Commandinfos = new HashSet<Commandinfo>();
            Guildusers = new HashSet<Guilduser>();
            Recruits = new HashSet<Recruit>();
            Roles = new HashSet<Role>();
            Vacations = new HashSet<Vacation>();
        }

        public int Id { get; set; }
        public ulong GuildId { get; set; }
        public DateTime CreateDate { get; set; }
        public string InitialName { get; set; }
        public ulong? RecruitmentChannelId { get; set; }

        public virtual ICollection<Blacklistentry> Blacklistentries { get; set; }
        public virtual ICollection<Commandinfo> Commandinfos { get; set; }
        public virtual ICollection<Guilduser> Guildusers { get; set; }
        public virtual ICollection<Recruit> Recruits { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<Vacation> Vacations { get; set; }
    }
}

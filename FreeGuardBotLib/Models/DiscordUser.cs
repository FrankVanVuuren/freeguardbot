﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models
{
    public partial class Discorduser
    {
        public Discorduser()
        {
            AliasCreateUsers = new HashSet<Alias>();
            AliasModifyUsers = new HashSet<Alias>();
            BlacklistentryCreateUsers = new HashSet<Blacklistentry>();
            BlacklistentryModifyUsers = new HashSet<Blacklistentry>();
            ConfirmationConfirmUsers = new HashSet<Confirmation>();
            ConfirmationCreateUsers = new HashSet<Confirmation>();
            ConfirmationModifyUsers = new HashSet<Confirmation>();
            Guildusers = new HashSet<Guilduser>();
            RecruitCreateUsers = new HashSet<Recruit>();
            RecruitModifyUsers = new HashSet<Recruit>();
            RecruitRecruitNavigations = new HashSet<Recruit>();
            RecruitRecruiters = new HashSet<Recruit>();
            RoleCreateUsers = new HashSet<Role>();
            RoleModifyUsers = new HashSet<Role>();
            RolerightCreateUsers = new HashSet<Roleright>();
            RolerightModifyUsers = new HashSet<Roleright>();
            VacationAffecteds = new HashSet<Vacation>();
            VacationCreateUsers = new HashSet<Vacation>();
            VacationModifyUsers = new HashSet<Vacation>();
        }

        public ulong DiscordUserId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual ICollection<Alias> AliasCreateUsers { get; set; }
        public virtual ICollection<Alias> AliasModifyUsers { get; set; }
        public virtual ICollection<Blacklistentry> BlacklistentryCreateUsers { get; set; }
        public virtual ICollection<Blacklistentry> BlacklistentryModifyUsers { get; set; }
        public virtual ICollection<Confirmation> ConfirmationConfirmUsers { get; set; }
        public virtual ICollection<Confirmation> ConfirmationCreateUsers { get; set; }
        public virtual ICollection<Confirmation> ConfirmationModifyUsers { get; set; }
        public virtual ICollection<Guilduser> Guildusers { get; set; }
        public virtual ICollection<Recruit> RecruitCreateUsers { get; set; }
        public virtual ICollection<Recruit> RecruitModifyUsers { get; set; }
        public virtual ICollection<Recruit> RecruitRecruitNavigations { get; set; }
        public virtual ICollection<Recruit> RecruitRecruiters { get; set; }
        public virtual ICollection<Role> RoleCreateUsers { get; set; }
        public virtual ICollection<Role> RoleModifyUsers { get; set; }
        public virtual ICollection<Roleright> RolerightCreateUsers { get; set; }
        public virtual ICollection<Roleright> RolerightModifyUsers { get; set; }
        public virtual ICollection<Vacation> VacationAffecteds { get; set; }
        public virtual ICollection<Vacation> VacationCreateUsers { get; set; }
        public virtual ICollection<Vacation> VacationModifyUsers { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib
{
    public class BotConfig
    {
        public string BotToken { get; set; }
        public string DbConnString { get; set; }

        private static BotConfig _instance;

        protected BotConfig()
        {

        }

        public static BotConfig GetInstance()
        {
            if (_instance is null)
            {
                string botToken = Environment.GetEnvironmentVariable("fg_bot_token");
                string dbConnString = Environment.GetEnvironmentVariable("fg_conn_string");

                _instance = new BotConfig();

                if (!string.IsNullOrEmpty(botToken) && !string.IsNullOrEmpty(dbConnString))
                {
                    _instance.BotToken = botToken;
                    _instance.DbConnString = dbConnString;
                }
                else
                {
                    throw new ArgumentException("Coult not find environment var for bot token and/or conn string!");
                }
            }

            return _instance;
        }
    }
}

﻿using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot
{
    public static class CommandHelpers
    {
        public static async Task LogCommandExecutionAsync(CommandContext ctx, string parameters = null)
        {
            using FreeGuardDbContext dbContext = new();

            var guild = await Guild.GetOrCreateGuildAsync(dbContext, ctx.Guild.Id, ctx.Guild.Name);
            await Commandinfo.LogCommandAsync(dbContext, guild.Id, ctx.Command.Name, parameters);
        }

        public static DiscordEmbed GetBlacklistEntryEmbed(FreeGuardDbContext dbContext, CommandContext cmdContext, Blacklistentry entry)
        {
            string userName = string.Empty;

            if (cmdContext.Guild.Members.TryGetValue(entry.CreateUserId, out DiscordMember value))
            {
                userName = value.DisplayName;
            }
            else
            {
                userName = entry.CreateUserId.ToString();
            }

            string playfab = !string.IsNullOrEmpty(entry.PlayfabId) ? entry.PlayfabId : "not set";

            var embedBuilder = new DiscordEmbedBuilder()
               .WithColor(new DiscordColor(0, 49, 83))
               .WithTitle($"Blacklistentry {entry.Id}:")
               .WithAuthor(cmdContext.Client.CurrentUser.Username)
               .AddField("Steamname", entry.Username)
               .AddField("SteamId", $"http://steamcommunity.com/profiles/{entry.SteamId}")
               .AddField("PlayfabId", playfab, true)
               .AddField("Reported by", userName, true)
               .AddField("Report day", entry.CreateDate.ToString("dd.MM.yyyy"), true)
               .AddField("Reason", entry.Reason);

            return embedBuilder.Build();
        }

        public static DiscordEmbed GetRecruitEmbed(DiscordGuild dsGuild, DiscordMember member, Recruit recruit, string botName)
        {
            string userName;
            if (dsGuild.Members.TryGetValue(recruit.RecruiterId.Value, out DiscordMember value))
            {
                userName = value.DisplayName;
            }
            else
            {
                userName = recruit.RecruitId.ToString();
            }

         var embedBuilder = new DiscordEmbedBuilder()
                .WithColor(new DiscordColor(0, 49, 83))
                .WithTitle($"Recruit No.{recruit.Id}")
                .WithAuthor(botName)
                .AddField("DiscordName", member.Mention)
                .AddField("Name", recruit.Name)
                .AddField("SteamId", $"http://steamcommunity.com/profiles/{recruit.SteamId}")
                .AddField("Recruiter", userName, true)
                .AddField("Recruit date", recruit.CreateDate.ToString("dd.MM.yyyy"), true);

            return embedBuilder.Build();
        }
    }
}

﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using FreeGuardBotLib.AccessManagement;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Attributes
{
    /// <summary>
    /// Checks whether a user has access to an command.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class CheckUserAccessAttribute : CheckBaseAttribute
    {
        private readonly IAccessManager _accessManager;

        /// <summary>
        /// The rights needed for a command to access it.
        /// </summary>
        public IEnumerable<Rights> NeededRights { get; }

        /// <summary>
        /// Standardconstructor
        /// </summary>
        public CheckUserAccessAttribute(params Rights[] rights)
        {
            _accessManager = new CommandAccessManager();

            NeededRights = rights;
        }

        /// <summary>
        /// Checks whether a user has access to a command asynchronously.
        /// </summary>
        /// <param name="ctx">The context of the command which should be checked.</param>
        /// <param name="help">actually dont know XD</param>
        /// <returns>True if the user has access false if not.</returns>
        public async override Task<bool> ExecuteCheckAsync(CommandContext ctx, bool help)
        {
            if (ctx.Guild != null)
            {
                return await _accessManager.CheckUserAccessAsync(ctx, NeededRights);
            }
            return false;
        }
    }
}

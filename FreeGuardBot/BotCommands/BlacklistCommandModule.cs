﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands.Constants;
using FreeGuardBot.BotCommands.Enums;
using FreeGuardBot.Converters;
using FreeGuardBotLib.AccessManagement;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands
{
    /// <summary>
    /// Contains the Commands for the blacklist.
    /// </summary>
    [Group("blacklist")]
    [Description("Commands for the blacklist.")]
    public class BlacklistCommandModule : BaseCommandModule
    {
        /// <summary>
        /// Command for creating new blacklist entries. Requests a lookup player string
        /// </summary>
        /// <param name="ctx">The context of the command.</param>
        /// <returns>Returns just a task.</returns>
        [RequireGuild]
        [CheckUserAccess(Rights.CreateEntry, Rights.ManageEntries, Rights.UseList, Rights.ManageBot)]
        [Command(BlacklistCommandNameConstants.CreateEntryCommand), Description("Command to create blacklist entries. Will open a dm channel. Please have a lookup player string ready.")]
        public async Task CreateEntryCommand(CommandContext ctx)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx);

            var directChannel = await ctx.Member.CreateDmChannelAsync();

            await directChannel.SendMessageAsync("Pls send the lookup player string!");
            var lookUpPlayer = await directChannel.GetNextMessageAsync();

            await directChannel.SendMessageAsync("Pls send the reason!");
            var reason = await directChannel.GetNextMessageAsync();

            ctx.Client.Logger.LogDebug($"LookupPlayer:{lookUpPlayer.Result.Content};Reason:{reason.Result.Content}");

            LookUpPlayerData lookUpPlayerData = new(lookUpPlayer.Result.Content);

            using FreeGuardDbContext dbContext = new();

            var guild = await dbContext.Guilds
                .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);
            var entry = await dbContext.Blacklistentries
                .SingleOrDefaultAsync(x => x.SteamId == lookUpPlayerData.SteamId && x.GuildId == guild.Id);

            if (entry is null)
            {
                entry = new Blacklistentry()
                {
                    Username = lookUpPlayerData.Name,
                    SteamId = lookUpPlayerData.SteamId,
                    PlayfabId = lookUpPlayerData.PlayFabId,
                    Reason = reason.Result.Content,
                    CreateDate = DateTime.UtcNow,
                    CreateUserId = ctx.Member.Id,
                    GuildId = guild.Id
                };

                await dbContext.AddAsync(entry);
                await dbContext.SaveChangesAsync();

                await directChannel.SendMessageAsync("New blacklist entry created!");

                await new DiscordMessageBuilder()
                    .WithEmbed(CommandHelpers.GetBlacklistEntryEmbed(dbContext, ctx, entry))
                    .SendAsync(ctx.Channel);
            }
            else
            {
                await directChannel.SendMessageAsync("The entry already exists!");
            }

            await ctx.Message.DeleteAsync("Bot deletion for cleanup");
        }

        /// <summary>
        /// Command for deleting entries on the blacklist. Does request a lookup player string.
        /// </summary>
        /// <param name="ctx">The context of the command.</param>
        /// <returns>Returns just a task.</returns>
        [RequireGuild]
        [CheckUserAccess(Rights.DeleteEntry, Rights.ManageBot, Rights.ManageEntries)]
        [Command(BlacklistCommandNameConstants.DeleteEntryCommand), Description("Command for deleting blacklist entries. This is not recommended.")]
        public async Task DeleteEntryCommand(CommandContext ctx)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx);

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

            var directChannel = await ctx.Member.CreateDmChannelAsync();

            await directChannel.SendMessageAsync("Pls send the lookup player string.");
            var lookUpPlayerString = await directChannel.GetNextMessageAsync();

            var lookUpPlayerData = new LookUpPlayerData(lookUpPlayerString.Result.Content);

            var blEntry = await Blacklistentry.GetAsync(dbContext, lookUpPlayerData.SteamId, guild.Id);

            await DeleteEntryAsync(blEntry, directChannel, dbContext, ctx);

            await ctx.Message.DeleteAsync();
        }

        /// <summary>
        /// Command for deleting blacklist entries. Does not request a lookup player string.
        /// </summary>
        /// <param name="ctx">The context of the command.</param>
        /// <param name="blacklistEntryId">The id of the blacklist entry which should be deleted.</param>
        /// <returns>Returns just a task.</returns>
        [RequireGuild]
        [CheckUserAccess(Rights.DeleteEntry, Rights.ManageBot, Rights.ManageEntries)]
        [Command(BlacklistCommandNameConstants.DeleteEntryCommand)]
        public async Task DeleteEntryCommand(CommandContext ctx, int blacklistEntryId)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx);

            using FreeGuardDbContext dbContext = new();

            var directChannel = await ctx.Member.CreateDmChannelAsync();

            var blEntry = await dbContext.Blacklistentries.FindAsync(blacklistEntryId);

            await DeleteEntryAsync(blEntry, directChannel, dbContext, ctx);

            await ctx.Message.DeleteAsync();
        }

        /// <summary>
        /// Command for updating blacklist entries.
        /// </summary>
        /// <param name="ctx">The context of the command.</param>
        /// <returns>Returns just a task.</returns>
        [RequireGuild]
        [CheckUserAccess(Rights.UpdateEntry, Rights.ManageBot, Rights.ManageEntries)]
        [Command(BlacklistCommandNameConstants.UpdateEntryCommand)]
        public async Task UpdateEntryCommand(CommandContext ctx)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx);

            await ctx.RespondAsync("Not implemented yet!");
        }

        /// <summary>
        /// Command for exporting the blacklist entries into a csv file.
        /// </summary>
        /// <param name="ctx">The context of the command.</param>
        /// <returns>Returns just a task.</returns>
        [RequireGuild]
        [CheckUserAccess(Rights.ExportList, Rights.ManageBot)]
        [Command(BlacklistCommandNameConstants.ExportListCommand)]
        public async Task ExportListCommand(CommandContext ctx)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx);

            await ctx.RespondAsync("Not implemented yet!");
        }

        /// <summary>
        /// Command for searching entries in the blacklist. Does request a lookup player string.
        /// </summary>
        /// <param name="ctx">The context of the command.</param>
        /// <returns>Returns just a task.</returns>
        [RequireGuild]
        [CheckUserAccess(Rights.SearchEntries, Rights.UseList, Rights.ManageBot, Rights.ManageEntries)]
        [Command(BlacklistCommandNameConstants.SearchEntryCommand)]
        public async Task SearchEntryCommand(CommandContext ctx)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx);

            var directChannel = await ctx.Member.CreateDmChannelAsync();

            await directChannel.SendMessageAsync("Pls send the lookup player string.");
            var lookUpPlayerMessage = await directChannel.GetNextMessageAsync();
            var lookUpPlayerData = new LookUpPlayerData(lookUpPlayerMessage.Result.Content);

            using var dbContext = new FreeGuardDbContext();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

            var entry = await Blacklistentry.GetAsync(dbContext, lookUpPlayerData.SteamId, guild.Id);

            if (entry != null)
            {
                var embed = CommandHelpers.GetBlacklistEntryEmbed(dbContext, ctx, entry);

                await ctx.RespondAsync("Found results: ");
                await ctx.Channel.SendMessageAsync(embed);
            }
            else
            {
                await ctx.RespondAsync("No entry found!");
            }
        }

        /// <summary>
        /// Command for searching blacklist entries. 
        /// </summary>
        /// <param name="ctx">The context of the command.</param>
        /// <param name="searchType">The search type.</param>
        /// <param name="searchValue">The search value.</param>
        /// <returns></returns>
        [RequireGuild]
        [CheckUserAccess(Rights.SearchEntries, Rights.UseList, Rights.ManageBot, Rights.ManageEntries)]
        [Command(BlacklistCommandNameConstants.SearchEntryCommand)]
        public async Task SearchEntryCommand(CommandContext ctx, SearchType searchType, string searchValue)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx);

            using var dbContext = new FreeGuardDbContext();

            int id = searchType switch
            {
                SearchType.EntryId => int.Parse(searchValue),
                _ => 0,
            };

            IEnumerable<Blacklistentry> searchResults = searchType switch
            {
                SearchType.SteamId => await dbContext.Blacklistentries
                    .Where(x => x.SteamId == searchValue)
                    .ToListAsync(),
                SearchType.PlayfabId => await dbContext.Blacklistentries
                    .Where(x => x.PlayfabId == searchValue)
                    .ToListAsync(),
                SearchType.IngameName => await dbContext.Blacklistentries
                    .Where(x => x.Username.Contains(searchValue))
                    .ToListAsync(),
                SearchType.EntryId => await dbContext.Blacklistentries
                    .Where(x => x.Id == id)
                    .ToListAsync(),
                _ => Enumerable.Empty<Blacklistentry>(),
            };

            if (searchResults.Any())
            {
                await ctx.RespondAsync("Found results: ");

                foreach (var searchResult in searchResults)
                {
                    var embed = CommandHelpers.GetBlacklistEntryEmbed(dbContext, ctx, searchResult);

                    await ctx.Channel.SendMessageAsync(embed);
                }
            }
            else
            {
                await ctx.RespondAsync("No entry found!");
            }

        }

        private async Task DeleteEntryAsync(Blacklistentry blEntry, DiscordChannel directChannel, DbContext dbContext, CommandContext ctx)
        {
            await directChannel.SendMessageAsync("Pls send a reason for the deletion.");
            var reason = await directChannel.GetNextMessageAsync();

            if (blEntry is null)
            {
                await directChannel.SendMessageAsync("Cant delete a non existing entry!");
            }
            else
            {
                var embed = new DiscordEmbedBuilder()
                    .WithColor(new DiscordColor(0, 49, 83))
                    .WithTitle($"*{ctx.Member.DisplayName}* deleted the entry *Nr.{blEntry.Id}*! Reason: *{reason.Result.Content}*")
                    .Build();

                dbContext.Remove(blEntry);
                await dbContext.SaveChangesAsync();

                await directChannel.SendMessageAsync("Deleted entry from blacklist!");

                await ctx.Message.Channel.SendMessageAsync(embed);
            }
        }
    }
}

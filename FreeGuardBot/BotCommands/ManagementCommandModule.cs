﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands.Constants;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands
{
    /// <summary>
    /// Contains the Commands for the blacklist.
    /// </summary>
    [Group("management")]
    [Description("Contains commands for managing the bot.")]
    public class ManagementCommandModule : BaseCommandModule
    {
        /// <summary>
        /// Command for viewing the rights of commands.
        /// </summary>
        /// <param name="ctx">The context of the command.</param>
        /// <returns>Returns just a task.</returns>
        [RequireGuild]
        [CheckUserAccess(Rights.ViewRights, Rights.ManageBot)]
        [Command(ManagementCommandNameConstants.ViewRightsCommand)]
        public async Task ViewRightsCommand(CommandContext ctx)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx);

            using FreeGuardDbContext dbContext = new();
            var rights = await dbContext.Botrights.ToListAsync();
            var guild = await dbContext.Guilds.SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);

            await Commandinfo.LogCommandAsync(dbContext, guild.Id, "blview-rights");

            var embedBuilder = new DiscordEmbedBuilder()
                .WithColor(new DiscordColor(255, 131, 0))
                .WithAuthor(ctx.Client.CurrentUser.Username)
                .WithTitle("Bot rights")
                .WithDescription("Displays the bots current right system. Admins and guild owners always have access to all commands.");

            await AddRightsToEmbedAsync(ctx, dbContext, guild.Id, rights, embedBuilder);

            await new DiscordMessageBuilder()
                .WithEmbed(embedBuilder.Build())
                .SendAsync(ctx.Channel);
        }

        /// <summary>
        /// Command for setting rigths to a specific role.
        /// </summary>   
        /// <param name="ctx">The context of the command.</param>
        /// <param name="botright">The rigths for the bot commands.</param>
        /// <param name="discordRole">The role which should get the right.</param>
        /// <returns>Returns just a task.</returns>
        [RequireGuild]
        [CheckUserAccess(Rights.SetRights, Rights.ManageBot)]
        [Command(ManagementCommandNameConstants.SetRightCommand)]
        public async Task SetRightCommand(CommandContext ctx, Rights botright, DiscordRole discordRole)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx, $"right:{botright};role:{discordRole.Id}");

            using FreeGuardDbContext dbContext = new();

            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);
            var role = await Role
                .GetOrCreateRoleAsync(dbContext, discordRole.Id, ctx.Member.Id, guild.Id);
            var right = await dbContext.Botrights
                .FindAsync((int)botright);
            var roleRight = await dbContext.Rolerights
                .SingleOrDefaultAsync(x => x.RoleId == role.Id && x.BotRightId == right.Id);

            if (roleRight is null)
            {
                roleRight = new Roleright()
                {
                    RoleId = role.Id,
                    BotRightId = right.Id,
                    CreateUserId = ctx.Member.Id,
                    CreateDate = DateTime.UtcNow
                };

                await dbContext.AddAsync(roleRight);
                await dbContext.SaveChangesAsync();

                await ctx.RespondAsync($"The role '{discordRole.Name}' has now the '{right.Name}' rights.");
            }
            else
            {
                await ctx.RespondAsync("This role already posses this right!");
            }
        }

        /// <summary>
        /// Command for deleting rights for bot commands.
        /// </summary>
        /// <param name="ctx">The context of the command.</param>
        /// <param name="botright">The right which should be deleted.</param>
        /// <param name="discordRole">The role which should lose this right.</param>
        /// <returns>Returns just a task.</returns>
        [RequireGuild]
        [CheckUserAccess(Rights.DeleteRights, Rights.ManageBot)]
        [Command(ManagementCommandNameConstants.DeleteRightCommand)]
        public async Task DeleteRightCommand(CommandContext ctx, Rights botright, DiscordRole discordRole)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx, $"right:{botright};role:{discordRole.Id}");

            using FreeGuardDbContext dbContext = new();

            var guild = await dbContext.Guilds
                .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);
            var role = await Role
                .GetOrCreateRoleAsync(dbContext, discordRole.Id, ctx.Member.Id, guild.Id);
            var right = await dbContext.Botrights
                .FindAsync((int)botright);
            var roleRight = await dbContext.Rolerights
                .SingleOrDefaultAsync(x => x.RoleId == role.Id && x.BotRightId == right.Id);

            if (roleRight != null)
            {
                dbContext.Rolerights.Remove(roleRight);
                await dbContext.SaveChangesAsync();

                await ctx.RespondAsync($"The right '{right.Name}' has been removed from the role '{discordRole.Name}'.");
            }
            else
            {
                await ctx.RespondAsync("This role does not have the searched right!");
            }
        }

        [RequireGuild]
        [CheckUserAccess(Rights.ManageBot)]
        [Command(ManagementCommandNameConstants.SetWarningRoleName)]
        public async Task SetWarningRoleCommand(CommandContext ctx, DiscordRole role)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx, role.Id.ToString());

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

            var warningRole = await Role.GetOrCreateRoleAsync(dbContext, role.Id, ctx.Member.Id, guild.Id);

            if (warningRole.HasWarning)
            {
                await ctx.RespondAsync($"The role **_'{role.Name}'_** already is set up as warning role!");

                return;
            }

            await SetWarningRoleAsync(dbContext, warningRole, ctx.Member.Id);

            await ctx.RespondAsync($"The role **_'{role.Name}'_** is now set as a warning role!");
        }

        [RequireGuild]
        [CheckUserAccess(Rights.ManageBot)]
        [Command(ManagementCommandNameConstants.DeleteWarningRoleName)]
        public async Task DeleteWarningRoleCommand(CommandContext ctx, DiscordRole role)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx, role.Id.ToString());

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

            var warningRole = await Role.GetOrCreateRoleAsync(dbContext, role.Id, ctx.Member.Id, guild.Id);

            if (!warningRole.HasWarning)
            {
                await ctx.RespondAsync($"The role **_'{role.Name}'_** has not the warning role!");

                return;
            }

            await DeleteWarningRoleAsync(dbContext, warningRole, ctx.Member.Id);

            await ctx.RespondAsync($"The warning role has been removed from the **_'{role.Name}'_** role!");
        }

        [RequireGuild]
        [CheckUserAccess(Rights.ManageBot)]
        [Command(ManagementCommandNameConstants.SetJoinRolesName)]
        public async Task SetJoinRolesCommand(CommandContext ctx, params DiscordRole[] roles)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx, roles.ToString());

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

            foreach (var role in roles)
            {
                var joinRole = await Role.GetOrCreateRoleAsync(dbContext, role.Id, ctx.Member.Id, guild.Id);

                if (joinRole.HasWarning)
                {
                    await ctx.RespondAsync($"The role **_'{role.Name}'_** already is set up as join role!");

                    continue;
                }

                await SetJoinRoleAsync(dbContext, joinRole, ctx.Member.Id);
                await ctx.RespondAsync($"The role **_'{role.Name}'_** is now set as a join role!");
            }
        }

        [RequireGuild]
        [CheckUserAccess(Rights.ManageBot)]
        [Command(ManagementCommandNameConstants.DeleteJoinRolesName)]
        public async Task DeleteJoinRolesCommand(CommandContext ctx, params DiscordRole[] roles)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx, roles.ToString());

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

            foreach (var role in roles)
            {
                var joinRole = await Role.GetOrCreateRoleAsync(dbContext, role.Id, ctx.Member.Id, guild.Id);

                if (joinRole.HasWarning)
                {
                    await ctx.RespondAsync($"The role **_'{role.Name}'_** has not the join role!");

                    continue;
                }

                await DeleteJoinRolesAsync(dbContext, joinRole, ctx.Member.Id);
                await ctx.RespondAsync($"The join role has been removed from the **_'{role.Name}'_** role!");
            }
        }

        [RequireGuild]
        [CheckUserAccess(Rights.ManageBot)]
        [Command(ManagementCommandNameConstants.SetRecruitmentChannelName)]
        public async Task SetRecruitmentChannelCommand(CommandContext ctx, DiscordChannel channel)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx, channel.Id.ToString());

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

            if (guild.RecruitmentChannelId == channel.Id)
            {
                await ctx.RespondAsync("Channel is already set as recruitment channel!");

                return;
            }

            await UpdateGuild(dbContext, guild, channel.Id);

            await ctx.RespondAsync("Channel has been set as recruitment channel!");
        }

        [RequireGuild]
        [CheckUserAccess(Rights.ManageBot)]
        [Command(ManagementCommandNameConstants.DeleteRecruitmentChannelName)]
        public async Task DeleteRecruitmentChannelCommand(CommandContext ctx, DiscordChannel channel)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx, channel.Id.ToString());

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

            if (!guild.RecruitmentChannelId.HasValue)
            {
                await ctx.RespondAsync("Channel is not set as recruitment channel!");

                return;
            }

            await UpdateGuild(dbContext, guild, null);

            await ctx.RespondAsync("Channel has been removed as recruitment channel! **Please dont forget to set a new one!**");
        }

        private async Task AddRightsToEmbedAsync(CommandContext ctx, FreeGuardDbContext dbContext, int guildId, List<Botright> rights, DiscordEmbedBuilder embedBuilder)
        {
            foreach (var right in rights)
            {
                embedBuilder.AddField($"{right.Id}. Right: {right.Name}", right.Description);

                var roleRights = await dbContext.Rolerights
                    .Include("Role")
                    .Where(x => x.BotRightId == right.Id)
                    .Where(x => x.Role.GuildId == guildId)
                    .ToListAsync();

                foreach (var roleRight in roleRights)
                {
                    if (ctx.Guild.Roles.TryGetValue(roleRight.Role.DiscordRoleId, out DiscordRole discordRole))
                    {
                        embedBuilder.AddField("Role name", discordRole.Name);
                    }
                    else
                    {
                        embedBuilder.AddField("Role Id:", roleRight.Role.DiscordRoleId.ToString());
                    }

                    embedBuilder.AddField("Create date: ", roleRight.CreateDate.ToString(), true);

                    if (ctx.Guild.Members.TryGetValue(roleRight.CreateUserId, out DiscordMember member))
                    {
                        embedBuilder.AddField("Create user: ", member.DisplayName, true);
                    }
                    else
                    {
                        embedBuilder.AddField("Create user: ", roleRight.CreateUserId.ToString(), true);
                    }

                    ctx.Client.Logger.LogDebug($"Role: {roleRight.Role.Id}");
                }
            }
        }

        private static async Task UpdateWarningRoleAsync(FreeGuardDbContext dbContext, Role warningRole, ulong userId, string isWarningText)
        {
            warningRole.IsWarningRole = isWarningText;
            warningRole.ModifyUserId = userId;
            warningRole.ModifyDate = DateTime.UtcNow;

            dbContext.Roles.Update(warningRole);
            await dbContext.SaveChangesAsync();
        }

        private static async Task UpdateJoinRoleAsync(FreeGuardDbContext dbContext, Role joinRole, ulong userId, string isJoinText)
        {
            joinRole.IsJoinRole = isJoinText;
            joinRole.ModifyUserId = userId;
            joinRole.ModifyDate = DateTime.UtcNow;

            dbContext.Roles.Update(joinRole);
            await dbContext.SaveChangesAsync();
        }

        private static async Task SetJoinRoleAsync(FreeGuardDbContext dbContext, Role joinRole, ulong userId)
        {
            await UpdateJoinRoleAsync(dbContext, joinRole, userId, "true");
        }

        private static async Task DeleteJoinRolesAsync(FreeGuardDbContext dbContext, Role joinRole, ulong userId)
        {
            await UpdateJoinRoleAsync(dbContext, joinRole, userId, "false");
        }

        private static async Task SetWarningRoleAsync(FreeGuardDbContext dbContext, Role joinRole, ulong userId)
        {
            await UpdateWarningRoleAsync(dbContext, joinRole, userId, "true");
        }

        private static async Task DeleteWarningRoleAsync(FreeGuardDbContext dbContext, Role joinRole, ulong userId)
        {
            await UpdateWarningRoleAsync(dbContext, joinRole, userId, "false");
        }

        private static async Task UpdateGuild(FreeGuardDbContext dbContext, Guild guild, ulong? channelId)
        {
            guild.RecruitmentChannelId = channelId;
            dbContext.Guilds.Update(guild);
            await dbContext.SaveChangesAsync();
        }
    }
}

﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands.Constants;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands
{
    [Group("recruit")]
    [Description("Commands for the recruitment.")]
    public class RecruitmentCommandModule : BaseCommandModule
    {
        [RequireGuild]
        [CheckUserAccess(Rights.CreateRecruit, Rights.ManageBot)]
        [Command(RecruitmentCommandNameConstants.CreateRecruitName)]
        public async Task CreateRecruitCommand(CommandContext ctx)
        {
            await CommandHelpers.LogCommandExecutionAsync(ctx);

            ulong recruiterId = ctx.Member.Id;

            var directChannel = await ctx.Member.CreateDmChannelAsync();

            await directChannel.SendMessageAsync("Pls send the steam id of the person to recruit.");
            var userMessage = await directChannel.GetNextMessageAsync(x => !x.Author.IsBot);
            string steamId = userMessage.Result.Content;

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

            if (CheckBlacklist(dbContext, guild.Id, steamId))
            {
                await directChannel.SendMessageAsync("This person is on the blacklist.");
                await directChannel.SendMessageAsync("Found entry:");

                var entry = await dbContext.Blacklistentries
                    .SingleOrDefaultAsync(x => x.GuildId == guild.Id && x.SteamId == steamId);
                var embed = CommandHelpers.GetBlacklistEntryEmbed(dbContext, ctx, entry);

                await directChannel.SendMessageAsync(embed);

                string confirmation = string.Empty;
                do
                {
                    await directChannel.SendMessageAsync("Are you sure you want to recruit this person? (yes/no)");
                    userMessage = await userMessage.Result.GetNextMessageAsync(x => !x.Author.IsBot);
                    confirmation = userMessage.Result.Content;
                } while (confirmation.ToLower() != "yes" && confirmation.ToLower() != "no");

                if (confirmation == "no")
                {
                    await directChannel.SendMessageAsync("Recruitment has been aborted!");

                    return;

                }
            }

            await directChannel.SendMessageAsync("Please give me the discord name.");
            userMessage = await userMessage.Result.GetNextMessageAsync(x => !x.Author.IsBot);
            string discordName = userMessage.Result.Content;

            await directChannel.SendMessageAsync("Please give me the ingame name.");
            userMessage = await userMessage.Result.GetNextMessageAsync(x => !x.Author.IsBot);
            string ingameName = userMessage.Result.Content;

            Recruit recruit = new()
            {
                Name = ingameName,
                DiscordName = discordName,
                SteamId = steamId,
                Joined = "false",
                RecruiterId = recruiterId,
                GuildId = guild.Id,
                CreateUserId = recruiterId,
                CreateDate = DateTime.UtcNow,
            };

            await dbContext.Recruits.AddAsync(recruit);
            await dbContext.SaveChangesAsync();

            await directChannel.SendMessageAsync("The recruit has been created, pls send him the invite following invite link:");
            var inviteLink = await ctx.Guild
                .GetDefaultChannel()
                .CreateInviteAsync(max_age: 0, max_uses: 1, reason: "Invite of new recruit.");

            await directChannel.SendMessageAsync(inviteLink.ToString());

            await directChannel.SendMessageAsync("As soon as the recruit joins he will get the appropariate rolls and will be displayed in recruiting.");
        }   

        private bool CheckBlacklist(FreeGuardDbContext context, int guildId, string steamId)
        {
            return context.Blacklistentries.Any(x => x.GuildId == guildId && x.SteamId == steamId);
        }
    }
}

﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands.Constants;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands
{
    /// <summary>
    /// Contains the Commands for the blacklist.
    /// </summary>
    [Group("support")]
    [Description("Commands for stuff like reporting bugs or making suggestions.")]
    public class SupportCommandModule : BaseCommandModule
    {
        [RequireGuild]
        [CheckUserAccess(Rights.ManageBot)]
        [Command(SupportCommandNameConstants.ReportBugCommandName), Description("Opens a dm channel in which you can report a bug, which will create an issue on gitlab.")]
        public async Task ReportBugCommand(CommandContext ctx)
        {
            await ctx.RespondAsync("Not implemented!");
        }

        [RequireGuild]
        [CheckUserAccess(Rights.ManageBot)]
        [Command(SupportCommandNameConstants.SuggestCommandName), Description("Opens a dm channel in which you can give a suggestion which will create an issue on gitlab.")]
        public async Task SuggestCommand(CommandContext ctx)
        {
            await ctx.RespondAsync("Not implemented!");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands.Enums
{
    /// <summary>
    /// Determines in what the search value will be searched.
    /// </summary>
    public enum SearchType
    {
        /// <summary>
        /// Searches the steam id.
        /// </summary>
        SteamId,
        /// <summary>
        /// Searches the playfab id.
        /// </summary>
        PlayfabId,
        /// <summary>
        /// Searches the ingame name.
        /// </summary>
        IngameName,
        /// <summary>
        /// Searches the entry id.
        /// </summary>
        EntryId
    }
}

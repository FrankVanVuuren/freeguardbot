﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands.Constants
{
    public static class ManagementCommandNameConstants
    {
        public const string SetWarningRoleName = "set-warn-role";
        public const string SetJoinRolesName = "set-join-roles";
        public const string DeleteJoinRolesName = "del-join-roles";
        public const string DeleteWarningRoleName = "del-warn-role";
        public const string SetRecruitmentChannelName = "set-recruit-channel";
        public const string DeleteRecruitmentChannelName = "del-recruit-channel";
        /// <summary>
        /// Constant for the ViewRightsCommand.
        /// </summary>
        public const string ViewRightsCommand = "view-rights";
        /// <summary>
        /// Constant for the SetRightCommand.
        /// </summary>
        public const string SetRightCommand = "set-right";
        /// <summary>
        /// Constant for the DeleteRightCommand.
        /// </summary>
        public const string DeleteRightCommand = "del-right";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands.Constants
{
    /// <summary>
    /// Constants for the command names.
    /// </summary>
    public static class BlacklistCommandNameConstants
    {
        /// <summary>
        /// Constant for the CreateEntryCommand.
        /// </summary>
        public const string CreateEntryCommand = "new";
        /// <summary>
        /// Constant for the UpdateEntryCommand.
        /// </summary>
        public const string UpdateEntryCommand = "update";
        /// <summary>
        /// Constant for the DeleteEntryCommand.
        /// </summary>
        public const string DeleteEntryCommand = "delete";
        /// <summary>
        /// Constant for the ExportListCommand.
        /// </summary>
        public const string ExportListCommand = "export";
        /// <summary>
        /// Constant for the SearchEntryCommand.
        /// </summary>
        public const string SearchEntryCommand = "search";
    }
}

﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Converters;
using DSharpPlus.Entities;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Converters
{
    class RightEnumConverter : IArgumentConverter<Rights>
    {
        public Task<Optional<Rights>> ConvertAsync(string value, CommandContext ctx)
        {
            if (Enum.TryParse(value, out Rights right))
            {
                return Task.FromResult(Optional.FromValue(right));
            }

            return Task.FromResult(Optional.FromNoValue<Rights>());
        }

        public Task<Optional<Rights>> ConvertAsync(int value, CommandContext _)
        {
            if (value >= 1 && value <= 11)
            {
                return Task.FromResult(Optional.FromValue((Rights)value));
            }

            return Task.FromResult(Optional.FromNoValue<Rights>());
        }
    }
}

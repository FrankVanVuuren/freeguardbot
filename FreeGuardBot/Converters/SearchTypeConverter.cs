﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Converters;
using DSharpPlus.Entities;
using FreeGuardBot.BotCommands.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Converters
{
    class SearchTypeConverter : IArgumentConverter<SearchType>
    {
        public Task<Optional<SearchType>> ConvertAsync(string value, CommandContext ctx)
        {
            return value.ToLower() switch
            {
                "steamid" => Task.FromResult(Optional.FromValue(SearchType.SteamId)),
                "playfabid" => Task.FromResult(Optional.FromValue(SearchType.PlayfabId)),
                "name" => Task.FromResult(Optional.FromValue(SearchType.IngameName)),
                "id" => Task.FromResult(Optional.FromValue(SearchType.EntryId)),
                _ => Task.FromResult(Optional.FromNoValue<SearchType>()),
            };
        }
    }
}

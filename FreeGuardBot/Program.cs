﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Exceptions;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands;
using FreeGuardBot.Converters;
using FreeGuardBotLib;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeGuardBot
{
    /// <summary>
    /// The entry class of the project.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The entry point into the program.
        /// </summary>
        /// <param name="commandIdentifier">The identifier used at the beginning of commands.</param>
        public static void Main(string commandIdentifier)
        {
            LLS();
            MainAsync(commandIdentifier).GetAwaiter().GetResult();
        }

        /// <summary>
        /// This simple message will strengthen and remind us of our ideals and maxims which we need to enhance
        /// the game experience day by day in matter of both small and great:Dealing with annoying abusers of
        /// the law and still following the rules or planning events and involving the community.
        /// Both needs strength, focus and patience which can be really nerve-wracking.
        /// </summary>
        static void LLS()
        {
            Console.WriteLine("▄                                  ▀▀█      ▀                                   ");
            Console.WriteLine("█       ▄▄▄   ▄ ▄▄    ▄▄▄▄           █    ▄▄▄    ▄   ▄   ▄▄▄                    ");
            Console.WriteLine("█      █▀ ▀█  █▀  █  █▀ ▀█           █      █    ▀▄ ▄▀  █▀  █                   ");
            Console.WriteLine("█      █   █  █   █  █   █           █      █     █▄█   █▀▀▀▀                   ");
            Console.WriteLine("█▄▄▄▄▄ ▀█▄█▀  █   █  ▀█▄▀█           ▀▄▄  ▄▄█▄▄    █    ▀█▄▄▀                   ");
            Console.WriteLine("                      ▄  █                                                      ");
            Console.WriteLine("                       ▀▀                                                       ");
            Console.WriteLine("                                                                                ");
            Console.WriteLine(" ▄▄▄▄    ▄           ▀▀█                    ▀        █    ▀                    ▄");
            Console.WriteLine("█▀   ▀ ▄▄█▄▄   ▄▄▄     █   ▄     ▄  ▄▄▄   ▄▄▄     ▄▄▄█  ▄▄▄     ▄▄▄   ▄▄▄▄▄    █");
            Console.WriteLine("▀█▄▄▄    █    ▀   █    █   ▀▄ ▄ ▄▀ █▀  █    █    █▀ ▀█    █    █   ▀  █ █ █    █");
            Console.WriteLine("    ▀█   █    ▄▀▀▀█    █    █▄█▄█  █▀▀▀▀    █    █   █    █     ▀▀▀▄  █ █ █    ▀");
            Console.WriteLine("▀▄▄▄█▀   ▀▄▄  ▀▄▄▀█    ▀▄▄   █ █   ▀█▄▄▀  ▄▄█▄▄  ▀█▄██  ▄▄█▄▄  ▀▄▄▄▀  █ █ █    █");
            Console.WriteLine("                                                                                ");
        }

        static async Task MainAsync(string commandIdentifier)
        {
            BotConfig config = BotConfig.GetInstance();

            var discord = new DiscordClient(new DiscordConfiguration()
            {
                Token = config.BotToken,
                TokenType = TokenType.Bot,
                Intents = DiscordIntents.All,
                MinimumLogLevel = Microsoft.Extensions.Logging.LogLevel.Debug
            });
            discord.UseInteractivity(new InteractivityConfiguration()
            {
                PollBehaviour = PollBehaviour.KeepEmojis,
                Timeout = TimeSpan.FromSeconds(60)
            });
            discord.GuildMemberAdded += GuildMemberJoinedHandler;

            var commands = discord.UseCommandsNext(new CommandsNextConfiguration()
            {
                StringPrefixes = new[] { commandIdentifier }
            });
            commands.CommandErrored += CmdErroredHandler;

            commands.RegisterCommands<BlacklistCommandModule>();
            commands.RegisterCommands<RecruitmentCommandModule>();
            commands.RegisterCommands<ManagementCommandModule>();
            commands.RegisterCommands<SupportCommandModule>();
            commands.RegisterConverter(new RightEnumConverter());
            commands.RegisterConverter(new SearchTypeConverter());

            await discord.ConnectAsync();
            await Task.Delay(-1);
        }

        private async static Task GuildMemberJoinedHandler(DiscordClient client, GuildMemberAddEventArgs e)
        {
            using FreeGuardDbContext dbContext = new();

            var guild = await Guild.GetAsync(dbContext, e.Guild.Id);
            var recruit = dbContext.Recruits
                .SingleOrDefault(x =>
                    x.GuildId == guild.Id &&
                    x.DiscordName == $"{e.Member.Username}#{e.Member.Discriminator}" &&
                    x.Joined == "false"
            );

            if (recruit != null)
            {
                try
                {
                    await GrantRecruitRolesAsync(dbContext, e, guild.Id);
                    await UpdateRecruitAsync(e, dbContext, guild, recruit);

                    if (guild.RecruitmentChannelId.HasValue)
                    {
                        await SendJoinMessage(e, recruit, guild.RecruitmentChannelId.Value, client.CurrentUser.Username);
                    }
                }
                catch (UnauthorizedAccessException ex)
                {
                    client.Logger.LogError(ex, "The bot either misses 'manage roles' permission or the right is higher in the hierachy.");
                }
            }
        }

        private async static Task SendJoinMessage(GuildMemberAddEventArgs e, Recruit recruit, ulong recruitmentChannelId, string botName)
        {
            var embed = CommandHelpers.GetRecruitEmbed(e.Guild, e.Member, recruit, botName);

            var channel = e.Guild.GetChannel(recruitmentChannelId);

            await new DiscordMessageBuilder()
                .WithEmbed(embed)
                .WithAllowedMention(new UserMention(e.Member.Id))
                .SendAsync(channel);
        }

        private static async Task UpdateRecruitAsync(GuildMemberAddEventArgs e, FreeGuardDbContext dbContext, Guild guild, Recruit recruit)
        {
            var recruited = await Discorduser.GetOrCreateDiscorduserAsync(dbContext, e.Member.Id);
            await Guilduser.GetOrCreateGuildUserAsync(dbContext, guild.Id, recruited.DiscordUserId);

            recruit.Joined = "true";
            recruit.RecruitId = recruited.DiscordUserId;
            dbContext.Recruits.Update(recruit);
            await dbContext.SaveChangesAsync();
        }

        private static async Task GrantRecruitRolesAsync(FreeGuardDbContext dbContext, GuildMemberAddEventArgs e, int guildId)
        {
            var joinRoles = await dbContext.Roles.Where(x => x.GuildId == guildId && x.IsJoinRole == "true").ToListAsync();

            foreach (var role in joinRoles)
            {
                if (e.Guild.Roles.TryGetValue(role.DiscordRoleId, out DiscordRole dsRole))
                {
                    await e.Member.GrantRoleAsync(dsRole, "Joining as new recruit.");
                }
            }
        }

        private static async Task CmdErroredHandler(CommandsNextExtension ctx, CommandErrorEventArgs e)
        {

            if (e.Exception is ChecksFailedException failedException)
            {
                var failedChecks = failedException.FailedChecks;

                foreach (var failedCheck in failedChecks)
                {
                    if (failedCheck is CheckUserAccessAttribute)
                    {
                        await e.Context.RespondAsync("You have insufficient rights for this command!");
                    }
                }
            }
            else
            {
                ctx.Client.Logger.LogError(e.Exception, "An error occured during command execution!");
                await e.Context.RespondAsync("An error occured during execution!");
            }
        }
    }
}

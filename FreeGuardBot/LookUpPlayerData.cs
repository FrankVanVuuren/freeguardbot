﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FreeGuardBot
{
    class LookUpPlayerData
    {
        private const string _regex = "PlayFabID: ([A-Z0-9]+), EntityID: ([A-Z0-9]+), Platform: Steam, PlatformAccountID: ([A-Z0-9]+), Name: (.+)";

        public string PlayFabId { get; set; }
        public string Name { get; set; }
        public string SteamId { get; set; }

        public LookUpPlayerData(string lookUpPlayerString)
        {
            Regex regex = new(_regex);

            var match = regex.Match(lookUpPlayerString);

            PlayFabId = match.Groups[1].Value;
            SteamId = match.Groups[3].Value;
            Name = match.Groups[4].Value;
        }
    }
}
